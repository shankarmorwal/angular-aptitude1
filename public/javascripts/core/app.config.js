/**
 * This is main config files.
 */
(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider'];

    function config($stateProvider, $urlRouterProvider, $locationProvider, $qProvider) {

        $qProvider.errorOnUnhandledRejections(false);
        //error file
        $urlRouterProvider.otherwise(function($injector, $location){
            var $state = $injector.get('$state');

            $state.go('error', { message: "Error 404! Page '" + $location.path() + "' not found." });
        });

        //routes
        $stateProvider
            //abstract route for todo
            .state('todo', {
                url: '/todo',
                abstract: true,
                views: {
                    '': {
                        templateUrl: 'templates/modules/template.html'
                    }
                }

            })
            .state('todo.list', {
                url: '/list',
                views: {
                    'mainView': {
                        templateUrl: 'templates/modules/todo/todo-list.html',
                        controller: 'listController as vm'
                    }
                }
            })
            .state('todo.add', {
                url: '/add',
                views: {
                    'mainView': {
                        templateUrl: 'templates/modules/todo/todo-add.html',
                        controller: 'listController as vm'
                    }
                }
            })
            .state('error', {
                url: '/error',
                templateUrl: 'templates/error.html',
                controller: 'errorController as vm',
                params: { message: "Page not found!" }
            });
        //enable html5 mode
        $locationProvider.html5Mode(true);
        //if route not found then redirect the application to this path.
        $urlRouterProvider.otherwise('/todo/list');
    }

})();