/**
 * this is main angular app module
 *Need to configure here the main dependencies
 */
(function () {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngCookies',
        'ngResource'
    ]);
})();