/**
 * Run method
 * this gets exculated when application starts. we can configure on route/state changes methods here
 */
(function () {
    'use strict';
    angular.module('app').run(run);

    run.$inject = [];

    function run() {
    }
})();