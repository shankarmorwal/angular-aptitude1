(function () {
    'use strict';

    angular
        .module('app')
        .directive('footer', footer);

    footer.$inject = [];

    function footer() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'templates/modules/footer/footer.html'
        };
    }
})();