/**
 * factory to interect to do module with backend.
 */
(function () {
    'use strict';
    angular
        .module('app')
        .factory('TodosService', TodosService);

    TodosService.$inject = ['$resource'];

    function TodosService($resource) {
        //create todo resource
        var Todo = $resource('/api/todos/:todoId', {
            todoId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });

        angular.extend(Todo.prototype, {
            createOrUpdate: function () {
                var todo = this;
                return createOrUpdate(todo);
            }
        });

        return Todo;
        
        /**
         * Create or update todo
         * @param todo
         * @param callback
         * @returns {*}
         */
        function createOrUpdate(todo, callback) {
            if (todo.id) {
                return todo.$update(onSuccess, onError);
            } else {
                return todo.$save(onSuccess, onError);
            }

            // Handle successful response
            function onSuccess(todo) {
            }

            // Handle error response
            function onError(errorResponse) {
                var error = errorResponse.data;
                // Handle error internally
            }
        }
    }
}());