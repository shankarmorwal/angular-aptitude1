(function () {
    'use strict';
    angular.module('app').controller('listController', listController);

    listController.$inject = ['TodosService','$state','$window'];

    function listController(TodosService, $state, $window) {
        /*jshint validthis: true */
        var vm = this;

        vm.selectedFilter = 'All';

        vm.todo = new TodosService();


        if($state.current.name == "todo.list") {
            vm.todos = TodosService.query();
        }


        vm.tempTodos = angular.copy(vm.todos);

        vm.save = saveOrUpdate;
        vm.remove = remove;
        /**
         * Save or update the task on the backend
         */
        function saveOrUpdate(info) {
            if(info)    vm.todo = new TodosService(info);
            vm.todo.createOrUpdate()
                .then(function(res){
                    $state.go('todo.list');
                })
                .catch(function(err){
                    console.log(err);
                });

        }

        // Remove existing Article
        function remove(info,$index) {
            var rmTodo = new TodosService(info);
            if ($window.confirm('Are you sure you want to delete?')) {
                rmTodo.$remove(function() {
                    vm.todos.splice($index, 1);
                });
            }
        }



    }
})();