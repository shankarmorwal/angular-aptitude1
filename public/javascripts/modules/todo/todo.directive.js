/**
 * Directive to show the task row
 */
(function () {
    'use strict';
    angular
        .module('app')
        .directive('todo', todo);

    todo.$inject = [];

    function todo() {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'templates/modules/todo/todo-view.html',
            scope : {
                'info': '=?',
                'remove': '&',
                'update': '=?'
            },
            controller: ['$scope', 'TodosService', '$window',todoController]
        };
    }
    /**
     * to do controller function
     * @param $scope
     * @param TodosService
     */

    function todoController ($scope, TodosService, $window) {
        //find days remaining days
        $scope.info.daysLeft = daysRemaining($scope.info.deadline);
        $scope.markToComplete = markToComplete;

        /**
         * mark the task completed.
         */
        function markToComplete() {
            //update task to backend

        }

        /**
         *
         * function to return days remaining string
         * @param d
         * @returns {string}
         */
        function daysRemaining(d) {
            var deadLine = moment(d);
            var today = moment();
            var difference = deadLine.diff(today, 'days');
            var returnString = 'Expired';
            $scope.info.difference = difference;
            //if else then 0 days then task is expiried
            if (difference < 0) {
                returnString = 'Expired';
            }
            //if diff is zero days then task is expiring today.
            else if (difference === 0) {
                returnString = 'Today';
            }
            else {
                //dealline is yet come
                returnString = 'In  ' + difference + ' days';
            }

            return returnString;
        }

    }

}());