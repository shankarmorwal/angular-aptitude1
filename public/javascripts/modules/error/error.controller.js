(function () {
    'use strict';
    angular.module('app').controller('errorController', errorController);

    errorController.$inject = ['$state'];

    function errorController($state) {
        /*jshint validthis: true */
        var vm = this;
        vm.message = $state.params.message;
    }
})();