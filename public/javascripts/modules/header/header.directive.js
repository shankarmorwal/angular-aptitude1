(function () {
    'use strict';

    angular
        .module('app')
        .directive('header', header);

    header.$inject = [];

    function header() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'templates/modules/header/header.html'
        };
    }
})();