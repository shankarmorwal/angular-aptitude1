var express = require('express');
var path = require('path');
var server = express();
var base = require('../routes/base');
var api = require('../routes/api');

server.use('/stylesheets', express.static(path.resolve(__dirname, '../public/stylesheets')));
server.use('/fonts', express.static(path.resolve(__dirname, '../public/fonts')));
server.use('/images', express.static(path.resolve(__dirname, '../public/images')));
server.use('/dist', express.static(path.resolve(__dirname, '../dist')));
server.use('/templates', express.static(path.resolve(__dirname, '../public/templates')));

server.use('/api', api);
server.use('/', base);

process.on('uncaughtException', function(err){
    console.error("Uncaught exception: ", err.stack);
});

process.on('unhandledRejection', function(err){
    console.error("Unhandled rejection", err.stack);
});

module.exports = server;
