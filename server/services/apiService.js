var Q = require('q');

var todoList = [
    {
        id: 1,
        name: "The first task",
        deadline: "2017-02-14T14:35:01Z",
        isCompleted : false,
        importance : 1
    },
    {
        id: 2,
        name: "The second task",
        deadline: "2017-01-01T14:35:01Z",
        isCompleted : false,
        importance : 2
    },
    {
        id: 3,
        name: "The third task",
        deadline: "2017-02-28T14:35:01Z",
        isCompleted : false,
        importance : 3
    },
    {
        id: 4,
        name: "The fourth task",
        deadline: "2018-01-01T14:35:01Z",
        isCompleted : false,
        importance : 4
    },
    {
        id: 5,
        name: "The fifth task",
        deadline: "2017-08-01T14:35:01Z",
        isCompleted : false,
        importance : 5
    }
];

module.exports = {

    getTodos: function() {
        var deferred = Q.defer();

        deferred.resolve(todoList);

        return deferred.promise;
    },

    newTodo : function(todo) {
        var deferred = Q.defer();

        todo.id = todoList[todoList.length - 1].id + 1;

        todoList.push(todo);
        deferred.resolve(todo);

        return deferred.promise;
    },

    update : function(params) {
        var deferred = Q.defer();

        var indexToUpdate = findOne(params.todoId);

        todoList[indexToUpdate].isCompleted = !todoList[indexToUpdate].isCompleted;

        deferred.resolve();

        return deferred.promise;

    },

    delete : function(params) {
        var deferred = Q.defer();

        var indexToUpdate = findOne(params.todoId);

        todoList.splice(indexToUpdate, 1);

        deferred.resolve();

        return deferred.promise;

    }
};

function findOne(id) {
    var index;
    for(var k=0; k < todoList.length; k++){
        if(todoList[k].id == id){
            index = k;
        }
    }
    return index;
}
