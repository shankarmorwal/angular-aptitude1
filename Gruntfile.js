var env = require('process').env;
var path = require('path');

module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-simple-mocha');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            options: {
                jshintrc: ".jshintrc"
            },
            all: [
                'public/javascripts/**/*.js',
                'public/partials/**/*.js',
                '*.js',
                'bin/**/*.js',
                'routes/**/*.js',
                'server/**/*.js',
                'test/**/*.js'
            ]
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd: 'public/stylesheets',
                    src: ['**/*.sass', '**/*.scss'],
                    dest: 'public/stylesheets',
                    ext: '.css'
                }]
            }
        },
        concat: {
            vendors_js: {
                src: [
                    'public/vendors/**/*.js',
                    'node_modules/angular/angular.js',
                    'node_modules/moment/moment.js',
                    'node_modules/angular-ui-router/release/angular-ui-router.js',
                    'node_modules/angular-cookies/angular-cookies.js',
                    'node_modules/angular-resource/angular-resource.min.js'
                ],
                dest: 'dist/vendors.js'
            },
            site_js: {
                src: [
                    'public/javascripts/core/app.module.js',
                    'public/javascripts/core/app.config.js',
                    'public/javascripts/core/app.constant.js',
                    'public/javascripts/core/app.run.js',
                    'public/javascripts/**/*.js',
                    'public/partials/**/*.js'
                ],
                dest: 'dist/<%= pkg.name %>.js'
            },
            vendors_css: {
                src: [
                    'node_modules/bootstrap/dist/css/bootstrap.css'
                ],
                dest: 'dist/vendors.css'
            },
            site_css: {
                src: [
                    'public/stylesheets/**/*.css'
                ],
                dest: 'dist/<%= pkg.name %>.css'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'dist/vendors.min.css': ['dist/vendors.css'],
                    'dist/<%= pkg.name %>.min.css': ['dist/<%= pkg.name %>.css']
                }
            }
        },
        uglify: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            vendors: {
                src: 'dist/vendors.js',
                dest: 'dist/vendors.min.js'
            },
            site: {
                src: 'dist/<%= pkg.name %>.js',
                dest: 'dist/<%= pkg.name %>.min.js'
            }
        },
        express: {
            options: {
                port: env.PORT || 3030,
                script: path.resolve(__dirname, 'bin/www')
            },
            dev: {
                options: {
                    background: true
                },
            },
            production: {
                options: {
                    background: false
                },
            }
        },
        watch: {
            build: {
                files: [
                    '*.js',
                    'server/**/*.js',
                    'public/**/*.html',
                    'public/**/*.js',
                    'public/**/*.css',
                    'public/**/*.sass',
                    'public/**/*.scss',
                    'config/**/*',
                    'routes/**/*',
                    'models/**/*.js'
                ],
                tasks: [
                    'jshint',
                    'dist',
                    'express:dev'
                ],
                options: {
                    livereload: false,
                    spawn: false
                }
            }
        },
        simplemocha: {
            backend: {
                src: 'test/backend/**/*.js'
            }
        },
        clean: {
            dist: ['dist/*'],
            css: ['public/stylesheets/**/*.css', 'public/stylesheets/**/*.css.map'],
        }
    });

    grunt.registerTask('test', ['jshint', 'simplemocha']);
    grunt.registerTask('dist', ['sass', 'concat:vendors_js', 'concat:site_js', 'concat:vendors_css', 'concat:site_css']);
    grunt.registerTask('distmin', ['dist', 'uglify', 'cssmin']);

    grunt.registerTask('default', ['jshint', 'dist', 'express:dev', 'watch']);
    grunt.registerTask('production', ['jshint', 'distmin', 'express:production']);
};