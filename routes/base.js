var express = require('express');
var base = express.Router();

base.get('/*', function(req, res){
    res.sendFile('index.html', { root: 'public' });
});

module.exports = base;
