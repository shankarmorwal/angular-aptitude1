var express = require('express');
var api = express.Router();
var apiService = require('../server/services/apiService');

api.get('/todos', function(req, res) {
    apiService.getTodos().then(function(todos) {
        res.send(todos);
    }).catch(function(error) {
        res.statusCode = 500;
        res.send({
            message: "Error getting todos!",
            error: error
        });
    });

});

api.post('/todos', function(req, res) {
    apiService.newTodo(req.body).then(function(todos) {
        res.statusCode = 200;
        res.send({
            message: "Created"
        });
    }).catch(function(error) {
        res.statusCode = 500;
        res.send({
            message: "Error getting todos!",
            error: error
        });
    });

});

api.put('/todos/:todoId', function(req, res) {
    apiService.update(req.params).then(function(todos) {
        res.statusCode = 200;
        res.send({
            message: "Updated"
        });
    }).catch(function(error) {
        console.log(error);
        res.statusCode = 500;
        res.send({
            message: "Error getting todos!",
            error: error
        });
    });

});

api.delete('/todos/:todoId', function(req, res) {
    apiService.delete(req.params).then(function(todos) {
        res.statusCode = 200;
        res.send({
            message: "Deleted"
        });
    }).catch(function(error) {
        console.log(error);
        res.statusCode = 500;
        res.send({
            message: "Error getting todos!",
            error: error
        });
    });

});

module.exports = api;
